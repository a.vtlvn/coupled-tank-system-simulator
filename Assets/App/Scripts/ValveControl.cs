using System;
using UnityEngine;

public class ValveControl : MonoBehaviour
{
    [field: SerializeField] public bool IsValveOpen;
    [SerializeField] private KeyCode _openKey;
    [SerializeField] private KeyCode _closeKey;
    [SerializeField] private ButtonEventHandler _openValveHandler;
    [SerializeField] private ButtonEventHandler _closeValveHandler;
    private bool _isValveOpen;
    private bool _isValveClose;

    public event Action<bool> OnValveStateChanged;
    
    private void OnEnable()
    {
        _openValveHandler.OnButtonStateChanged += OnValveOpen;
        _closeValveHandler.OnButtonStateChanged += OnValveClose;
    }

    private void OnDisable()
    {
        _openValveHandler.OnButtonStateChanged -= OnValveOpen;
        _closeValveHandler.OnButtonStateChanged -= OnValveClose;
    }
    
    private void Update()
    {
        if (Input.GetKey(_openKey) || _isValveOpen) 
            SetOpenState();
        
        if (Input.GetKey(_closeKey) || _isValveClose) 
            SetCloseState();
    }

    private void SetOpenState()
    {
        IsValveOpen = true;
        OnValveStateChanged?.Invoke(IsValveOpen);
    }
    
    private void SetCloseState()
    {
        IsValveOpen = false;
        OnValveStateChanged?.Invoke(IsValveOpen);
    }
    
    private void OnValveOpen(bool state)
    {
        _isValveOpen = state;
    }
    
    private void OnValveClose(bool state)
    {
        _isValveClose = state;
    }
}
