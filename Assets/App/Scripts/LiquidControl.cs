using UnityEngine;

public class LiquidControl : MonoBehaviour
{ 
    [SerializeField] private Liquid _liquid;
    [SerializeField] private KeyCode _increaseKey;
    [SerializeField] private KeyCode _decreaseKey;
    [SerializeField] private float _speed;
    [SerializeField] private ButtonEventHandler _increaseWaterLevelHandler;
    [SerializeField] private ButtonEventHandler _decreaseWaterLevelHandler;
    private bool _isWaterIncrease;
    private bool _isWaterDecrease;
   
    private void OnEnable()
    {
        _increaseWaterLevelHandler.OnButtonStateChanged += OnIncreaseButtonStateChanged;
        _decreaseWaterLevelHandler.OnButtonStateChanged += OnDecreaseButtonStateChanged;
    }

    private void OnDisable()
    {
        _increaseWaterLevelHandler.OnButtonStateChanged -= OnIncreaseButtonStateChanged;
        _decreaseWaterLevelHandler.OnButtonStateChanged -= OnDecreaseButtonStateChanged;
    }
    
    private void Update()
    {
        if (Input.GetKey(_increaseKey) || _isWaterIncrease)
        {
            ChangeFillAmount(-_speed * Time.deltaTime);
        }

        if (Input.GetKey(_decreaseKey) || _isWaterDecrease)
        {
            ChangeFillAmount(_speed * Time.deltaTime);
        }
    }

    private void ChangeFillAmount(float amount)
    {
        _liquid.FillAmount += amount;
        _liquid.FillAmount = Mathf.Clamp01(_liquid.FillAmount);
    }
    
    private void OnIncreaseButtonStateChanged(bool state)
    {
        _isWaterIncrease = state;
    }
    
    private void OnDecreaseButtonStateChanged(bool state)
    {
        _isWaterDecrease = state;
    }
}
