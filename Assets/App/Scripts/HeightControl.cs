using UnityEngine;

public class HeightControl : MonoBehaviour
{
    [field: SerializeField] public float HeightPercentage { get; private set; }
    [field: SerializeField] public float MaxHeight { get; private set; }
    [field: SerializeField] public float MinHeight{ get; private set; }
    [SerializeField] private float _speed;
    [SerializeField] private ButtonEventHandler _increasePipeHandler;
    [SerializeField] private ButtonEventHandler _decreasePipeHandler;
    private bool _isPipeIncrease;
    private bool _isPipeDecrease;

    private const float MAX_PERCENT = 100;
    
    private void OnEnable()
    {
        _increasePipeHandler.OnButtonStateChanged += OnIncreasePipe;
        _decreasePipeHandler.OnButtonStateChanged += OnDecreasePipe;
    }

    private void OnDisable()
    {
        _increasePipeHandler.OnButtonStateChanged -= OnIncreasePipe;
        _decreasePipeHandler.OnButtonStateChanged -= OnDecreasePipe;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.E) || _isPipeIncrease)
        {
            float newY = transform.position.y + _speed * Time.deltaTime;
            newY = Mathf.Clamp(newY, MinHeight, MaxHeight);
            transform.position = new Vector3(transform.position.x, newY, transform.position.z);
        }
        
        if (Input.GetKey(KeyCode.F) || _isPipeDecrease)
        {
            float newY = transform.position.y - _speed * Time.deltaTime;
            newY = Mathf.Clamp(newY, MinHeight, MaxHeight);
            transform.position = new Vector3(transform.position.x, newY, transform.position.z);
        }
        
        HeightPercentage = Mathf.InverseLerp(MinHeight, MaxHeight, transform.position.y) * MAX_PERCENT;
    }
    
    private void OnIncreasePipe(bool state)
    {
        _isPipeIncrease = state;
    }
    
    private void OnDecreasePipe(bool state)
    {
        _isPipeDecrease = state;
    }
}
