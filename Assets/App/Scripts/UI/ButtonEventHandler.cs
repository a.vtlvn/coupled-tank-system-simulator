using System;
using UnityEngine;

public class ButtonEventHandler : MonoBehaviour
{
    public event Action<bool> OnButtonStateChanged; 
    private bool _isPressed;

    public void Pressed()
    {
        _isPressed = true;
        RiseUp();
    }

    public void Unpressed()
    {
        _isPressed = false;
        RiseUp();
    }

    private void RiseUp()
    {
        OnButtonStateChanged?.Invoke(_isPressed);
    }
}
