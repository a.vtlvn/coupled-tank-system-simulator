using TMPro;
using UnityEngine;

public class WaterLevelDisplayer : MonoBehaviour
{
    [SerializeField] private TMP_Text _waterLevelText;
    [SerializeField] private string _waterTankId;
    [SerializeField] private Liquid _liquid;
    [SerializeField] private CanvasGroup _canvasGroup;
    private bool _isWaterDisplay;

    private void Start()
    {
        ChangeTextVisibility();
    }

    private void Update()
    {
        if (!_isWaterDisplay)
            return;

        ShowInfo();
    }

    public void ChangeState()
    {
        _isWaterDisplay = !_isWaterDisplay;
        ChangeTextVisibility();
    }

    private void ShowInfo()
    {
        string percent = (1f - _liquid.FillAmount).ToString("P");
        _waterLevelText.text = $"{_waterTankId}: {percent}";
    }

    private void ChangeTextVisibility()
    {
        _canvasGroup.alpha = !_isWaterDisplay ? 0 : 1;
    }
}
