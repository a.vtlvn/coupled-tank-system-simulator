using TMPro;
using UnityEngine;

public class ValveStateUI : MonoBehaviour
{
    [SerializeField] private TMP_Text _valveStateText;
    [SerializeField] private ValveControl _valveControl;
 
    
    private void Start()
    {
        OnValveStateChanged(_valveControl.IsValveOpen);
    }
    
    private void OnEnable()
    {
        _valveControl.OnValveStateChanged += OnValveStateChanged;
    }

    private void OnDisable()
    {
        _valveControl.OnValveStateChanged -= OnValveStateChanged;
    }

    private void OnValveStateChanged(bool valveIsOpen)
    {
        _valveStateText.text = $"Valve is open {valveIsOpen}";
    }
}
