using UnityEngine;

public class ClickOnCollider : MonoBehaviour
{
    [SerializeField] private WaterLevelDisplayer _waterLevelDisplayer;
    
    private void OnMouseDown()
    {
        _waterLevelDisplayer.ChangeState();
    }
}
