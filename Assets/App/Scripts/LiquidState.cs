using System.Collections;
using UnityEngine;

public class LiquidState : MonoBehaviour
{
    [SerializeField] private Liquid _liquidTank1;
    [SerializeField] private Liquid _liquidTank2;
    [SerializeField] private HeightControl _heightControl;
    [SerializeField] private ValveControl _valveControl;

    private float _previousTank1FillAmount;
    private float _previousTank2FillAmount;
    private float _previousValveHeight;

    private const float FILL_AMOUNT_EPSILON = 0.2f;
    private const float UPDATE_TIME = 0.1f;

    private void Start()
    {
        _previousTank1FillAmount = 1f - _liquidTank1.FillAmount;
        _previousTank2FillAmount = 1f - _liquidTank2.FillAmount;
        _previousValveHeight = GetClampedValue01(_heightControl.HeightPercentage);

        StartCoroutine(UpdateLiquidStateRoutine());
    }

    private IEnumerator UpdateLiquidStateRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(UPDATE_TIME);

            bool isValveOpen = _valveControl.IsValveOpen;
            float connectionHeight = GetClampedValue01(_heightControl.HeightPercentage);

            if (isValveOpen && connectionHeight >= 0f && connectionHeight <= 1f)
            {
                float tank1FillAmount = 1f - _liquidTank1.FillAmount;
                float tank2FillAmount = 1f - _liquidTank2.FillAmount;

                if (HasFillAmountChanged(tank1FillAmount, tank2FillAmount, connectionHeight))
                {
                    if (tank1FillAmount > tank2FillAmount && connectionHeight < tank1FillAmount)
                    {
                        TransferLiquid(tank1FillAmount, tank2FillAmount, connectionHeight);
                        AdjustLiquidLevels();
                    }
                    if (tank2FillAmount > tank1FillAmount && connectionHeight < tank2FillAmount)
                    {
                        TransferLiquid(tank2FillAmount, tank1FillAmount, connectionHeight);
                        AdjustLiquidLevels();
                    }

                    _previousTank1FillAmount = tank1FillAmount;
                    _previousTank2FillAmount = tank2FillAmount;
                    _previousValveHeight = connectionHeight;
                }
            }
        }
    }

    private bool HasFillAmountChanged(float tank1FillAmount, float tank2FillAmount, float connectionHeight)
    {
        return _previousTank1FillAmount != tank1FillAmount || _previousTank2FillAmount != tank2FillAmount || _previousValveHeight != connectionHeight;
    }

    private void TransferLiquid(float sourceTankFillAmount, float targetTankFillAmount, float connectionHeight)
    {
        float amountToTransfer = Mathf.Min(sourceTankFillAmount - targetTankFillAmount, connectionHeight);

        float updatedSourceFillAmount = 1f - (sourceTankFillAmount - amountToTransfer);
        float updatedTargetFillAmount = 1f - (targetTankFillAmount + amountToTransfer);

        if (Mathf.Abs(updatedSourceFillAmount - updatedTargetFillAmount) < FILL_AMOUNT_EPSILON)
        {
            float averageFillAmount = (sourceTankFillAmount + targetTankFillAmount) * 0.5f;
            updatedSourceFillAmount = 1f - averageFillAmount;
            updatedTargetFillAmount = 1f - averageFillAmount;
        }

        if (sourceTankFillAmount > targetTankFillAmount)
        {
            _liquidTank1.FillAmount = updatedSourceFillAmount;
            _liquidTank2.FillAmount = updatedTargetFillAmount;
        }
        else
        {
            _liquidTank1.FillAmount = updatedTargetFillAmount;
            _liquidTank2.FillAmount = updatedSourceFillAmount;
        }
    }

    private void AdjustLiquidLevels()
    {
        float tank1FillAmount = 1f - _liquidTank1.FillAmount;
        float tank2FillAmount = 1f - _liquidTank2.FillAmount;

        if (Mathf.Abs(tank2FillAmount - tank1FillAmount) < 0.4f)
        {
            float averageFillAmount = (tank1FillAmount + tank2FillAmount) * 0.5f;
            _liquidTank1.FillAmount = 1f - averageFillAmount;
            _liquidTank2.FillAmount = 1f - averageFillAmount;
        }
    }

    private float GetClampedValue01(float currentValue)
    {
        return Mathf.InverseLerp(0, 100, currentValue);
    }
}
