using System;
using UnityEngine;

[ExecuteInEditMode]
public class Liquid : MonoBehaviour
{ 
    [field: SerializeField] public float FillAmount;

    private enum UpdateMode { Normal, UnscaledTime }
    [SerializeField] private UpdateMode updateMode;
    
    [SerializeField] private Mesh _mesh;
    [SerializeField] private Renderer _rend;
    [SerializeField] private float _maxWobble = 0.03f;
    [SerializeField] private float _wobbleSpeedMove = 1f;
    [SerializeField] private float _recovery = 1f;
    [SerializeField] private float _thickness = 1f;
    [Range(0, 1)] private float _compensateShapeAmount;
   
    private Vector3 _pos;
    private Vector3 _lastPos;
    private Vector3 _velocity;
    private Vector3 _angularVelocity;
    private Vector3 _comp;
    private Quaternion _lastRot;
    private float _wobbleAmountX;
    private float _wobbleAmountZ;
    private float _wobbleAmountToAddX;
    private float _wobbleAmountToAddZ;
    private float _pulse;
    private float _sinewave;
    private float _time = 0.5f;
    private float _gain;
    private const float MAX_SINEWAVE = 10;
    private const float WOOBLE_AMPLIFIER = 0.2f;
    private const float GAIN_AMPLIFIER = 2f;

    private void Update()
    {
        float deltaTime = 0;
        switch (updateMode)
        {
            case UpdateMode.Normal:
                deltaTime = Time.deltaTime;
                break;

            case UpdateMode.UnscaledTime:
                deltaTime = Time.unscaledDeltaTime;
                break;
        }

        _time += deltaTime;

        if (deltaTime != 0)
        {
            _wobbleAmountToAddX = Mathf.Lerp(_wobbleAmountToAddX, 0, (deltaTime * _recovery));
            _wobbleAmountToAddZ = Mathf.Lerp(_wobbleAmountToAddZ, 0, (deltaTime * _recovery));

            _pulse = 2 * Mathf.PI * _wobbleSpeedMove;
            _sinewave = Mathf.Lerp(_sinewave, Mathf.Sin(_pulse * _time), deltaTime * Mathf.Clamp(_velocity.magnitude + _angularVelocity.magnitude, _thickness, MAX_SINEWAVE));

            _wobbleAmountX = _wobbleAmountToAddX * _sinewave;
            _wobbleAmountZ = _wobbleAmountToAddZ * _sinewave;

            _velocity = (_lastPos - transform.position) / deltaTime;

            _angularVelocity = GetAngularVelocity(_lastRot, transform.rotation);

            _wobbleAmountToAddX += Mathf.Clamp((_velocity.x + (_velocity.y * WOOBLE_AMPLIFIER) + _angularVelocity.z + _angularVelocity.y) * _maxWobble, -_maxWobble, _maxWobble);
            _wobbleAmountToAddZ += Mathf.Clamp((_velocity.z + (_velocity.y * WOOBLE_AMPLIFIER) + _angularVelocity.x + _angularVelocity.y) * _maxWobble, -_maxWobble, _maxWobble);
        }

        _rend.sharedMaterial.SetFloat("_WobbleX", _wobbleAmountX);
        _rend.sharedMaterial.SetFloat("_WobbleZ", _wobbleAmountZ);

        UpdatePos(deltaTime);

        _lastPos = transform.position;
        _lastRot = transform.rotation;
    }

    private void UpdatePos(float deltaTime)
    {
        Vector3 worldPos = transform.TransformPoint(new Vector3(_mesh.bounds.center.x, _mesh.bounds.center.y, _mesh.bounds.center.z));
        
        if (_compensateShapeAmount > 0)
        {
            if (deltaTime != 0)
            {
                _comp = Vector3.Lerp(_comp, (worldPos - new Vector3(0, GetHighestPoint(), 0)), deltaTime * 10);
            }
            else
            {
                _comp = (worldPos - new Vector3(0, GetHighestPoint(), 0));
            }
            
            _pos = worldPos - transform.position - new Vector3(0, FillAmount * (_comp.y * _compensateShapeAmount), 0);
        }
        else
        {
            _pos = worldPos - transform.position - new Vector3(0, FillAmount * (GetHighestPoint() - GetLowestPoint()), 0);
        }
        
        _rend.sharedMaterial.SetVector("_FillAmount", _pos);
    }

    private Vector3 GetAngularVelocity(Quaternion foreLastFrameRotation, Quaternion lastFrameRotation)
    {
        var q = lastFrameRotation * Quaternion.Inverse(foreLastFrameRotation);
       
        if (Mathf.Abs(q.w) > 1023.5f / 1024.0f) 
            return Vector3.zero;
        
        if (q.w < 0.0f)
        {
            var angle = Mathf.Acos(-q.w);
            _gain = -GAIN_AMPLIFIER * angle / (Mathf.Sin(angle) * Time.deltaTime);
        }
        else
        {
            var angle = Mathf.Acos(q.w);
            _gain = 2.0f * angle / (Mathf.Sin(angle) * Time.deltaTime);
        }
        
        Vector3 angularVelocity = new Vector3(q.x * _gain, q.y * _gain, q.z * _gain);

        if (float.IsNaN(angularVelocity.z))
        {
            angularVelocity = Vector3.zero;
        }
        
        return angularVelocity;
    }

    private float GetHighestPoint()
    {
        float highestY = float.MinValue;
        Vector3 highestVert = Vector3.zero;
        Vector3[] vertices = _mesh.vertices;

        for (int i = 0; i < vertices.Length; i++)
        {
            Vector3 position = transform.TransformPoint(vertices[i]);

            if (position.y > highestY)
            {
                highestY = position.y;
                highestVert = position;
            }
        }
        
        return highestVert.y;
    }

    private float GetLowestPoint()
    {
        float lowestY = float.MaxValue;
        Vector3 lowestVert = Vector3.zero;
        Vector3[] vertices = _mesh.vertices;

        for (int i = 0; i < vertices.Length; i++)
        {
            Vector3 position = transform.TransformPoint(vertices[i]);

            if (position.y < lowestY)
            {
                lowestY = position.y;
                lowestVert = position;
            }
        }
        return lowestVert.y;
    }
}